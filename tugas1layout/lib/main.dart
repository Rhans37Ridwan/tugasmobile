import 'dart:ui';
import 'package:flutter/material.dart';
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
@override
Widget build(BuildContext context) {
return MaterialApp(
home: Scaffold(
backgroundColor: Colors.black,
appBar: AppBar(
backgroundColor: Colors.black,
title: Text(
'Aplikasi Pertama Muhammad Ridwan',
style: TextStyle(color: Colors.blue),
),
leading: Icon(
Icons.tab,
color: Colors.yellow,
),
actions: <Widget>[
Icon(
Icons.thumb_up,
color: Colors.cyan,
),
Padding(
padding: EdgeInsets.only(
right: 30,
),
),
Icon(
Icons.thumb_down,
color: Colors.red,
),
],
),
body: Container(
color: Colors.black,
child: Column(
children: <Widget>[
Image(
image: AssetImage('assets/ridwan.jpg'),
),
Text('Muhammad Ridwan',
style: TextStyle(color: Colors.red, fontSize: 25.0)),
],
),
),
));
}
}